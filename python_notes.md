# PythonLearning

Notes for Python tutorial.

## Python Format

Visit [here](https://www.python.org/dev/peps/pep-0008/) for Python formatting tips.

### Common Conventions

- use pylint
- 4 spaces for indent
- 80 line limit
- import on different lines
- avoid accessive spaces
- capitalize comments, full sentences
- naming conventions:
  - variables_naming_convention
  - ClassNamingConvention
  - function_naming_convention
  - method_naming_convention
  - CONSTANT_NAMING_CONVENTION
  - packagenamingconvention
  - module_naming_convention.py
- use clear names
- use the fact that empty sequences(lists, tuples) are false
- break before binary operator in math, ex

```python
total = (first_variable
         + second_variable
         - third_variable)
```

## Variables

### Data Types

Variables are dynamically typed.

Python has 5 standard data types: Numbers, String, List, Tuple and Dictionary.

Python numbers also come in: int (signed integers), long (long integers, they can also be represented in octal and hexadecimal), float (floating point real values) and complex (complex numbers).

Lists, sets, dictionaries and objects are mutiable. Everything else is not.

### Booleans

Any value with content can be evaluated to be true.

Non-empty strings and non-zero numbers can be evaluated to be true.

Non-empty tuples, sets, lists and dictionaries are true.

### Loops

You can _for_ loop through arrays, Strings, sets and range().

You can _for_ loop through arrays with index by `for val, i in enumerate(list)`

You can _for_ loop through dictionaries by `for key, val in dic.items()`

### Lists

Lists can store mutiple data types.

Create a list of increasing numbers with `range()`.

Create a list of n zeros with `n * [0]`.

You can turn any iterable into lists with `list()`.

You can _unpack_ an iterable by using the iterable unpacking operator _\*_ `first, *others, last = list`

You can do the same as above with list comprehension. BEST WAY.

Ex `[item[1] for item in items]`.

Ex `[item[1] for item in items if item[0] > 0]`.

Use `zip()` to combine two iterables together. The length of the return list the the length of the smallest iterable.

Visit [here](https://www.w3schools.com/python/python_ref_list.asp) for List methods.

## Other Operations

### String Operations

Strings are mutiable. You can use negative indexes.

You can use formated strings with `f"{string1}"`

Visit [here](https://www.w3schools.com/python/python_ref_string.asp) for String methods.

## Built-in-functions

Visit [here](https://www.w3schools.com/python/python_ref_functions.asp) for Built-in-functions.

### Math Operations

Visit [here](https://www.w3schools.com/python/module_math.asp) for Math methods.

### Ternary Operator

`message = "Good morning!" if time == "day" else "Good night!"`

## Functions

### Lambda Functions

Lambda functions are anonymous functions that can be passes into other functions like `map()`, `sort()`, `filter() `. Ex. `list.sort(key=lambda item:item[0])`

## Stacks

Stacks is a data structure that follows the Last-In-First-Out principle.

Stacks can be easily implemented in Python using lists, `pop()` and `append()`.

Python also has a standard lib, `collection.deque`

## Queues

Queues is a data structure that follows the First-In-First-Out principle.

Queues can be easily implemented in Python using lists, `pop(0)` and `append()`.

Python also has a standard lib, `collection.deque`

## Generator Objects

Generators are iterable objects that are not stored in memory. These objects are good for iteration over a infinite stream of data or a huge list that does not need to be stored in memory.

## Exceptions

Python support exceptions with `try, except, else, finally` keywords.

`else` is executed if no exception was thrown.

`finally` is executed no matter what.

Sometimes you can use `with`

```python
with open("file.txt") as file:
    print("file opened")
```

to open files, etc. The with keyword will automatically release external resources used. Only objects with `__enter__, __exit__` methods can be used with `with`.

You can raise an exception with the raise keyword but is costly. Another approach with the best performance is to return a `None` value. The return value can be checked to see if an error has occured.

## Classes

### Attributes

Create a class with the `class` keyword.

Any methods of a class must have `self` as a parameter.

The constructor of a class is written in the `__init__` method.

Attributes can be added after like Javascript, ex. `obj.x = 3`. We do not have to define all attributes in the constructor like Java.

Variables defined in the constructor are called instance attributes; they are difference for each instance. Variables defined in the class level are called class attributes, they are initialized the same but can still be changed.

#### Private Attributes

To make a attribute private, add double underscore in front of its name in the constructor.

### Properties

Use properties to replace getters and setters in classes for a cleaner codebase.

### Methods

Just like attributes, we can have class methods as well. It is common to pass `cls` as a parameter and decorate it with `@classmethod`. `cls` is the pointer to the class object.

#### Magic Methods

Magic methods are methods Python use to determine how an object is used. For ex. `__str__(self)` methods defines how to convert your object to a string.

Visit [here](https://rszalski.github.io/magicmethods/) for all Magic methods.

### Inheritance

Classes can be inherited using definition `class Child(Parent):`.

Parent attributes are automatically inherited but remeber to call the constructor of the parent class in the child class. `super().__init__()`

Python supports mutiple inheritance but can cause issues if not used properly.

## Modules

Python modules are mapped to files and are imported with the `import` keyword.

The module name is the file name. When a module is imported, all functions and global variables can be accessed.

You can rename modules with the `as` keyword.

### Compiled Files

Compiled versions of modules are saved in the `__pycache__` folder. This allows for faster load times from skipping compile times.

## Packages

Python packages are mapped to folders that contain one or more modules. They are initialized with `__init__.py`.

Ex. `from ecommerce import sales` or `import ecommerce.sales`

### PYPI

Use Pypi to find additional packages outside of the standard library.
